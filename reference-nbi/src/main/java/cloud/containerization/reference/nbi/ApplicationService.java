package cloud.containerization.reference.nbi;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//import javax.inject.Inject;
//import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
//import org.springframework.test.context.transaction.TransactionConfiguration;
//import org.springframework.transaction.annotation.Propagation;
//import org.springframework.transaction.annotation.Transactional;

import cloud.containerization.reference.entity.Device;
import cloud.containerization.reference.entity.Record;
import cloud.containerization.reference.entity.User;



//@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = true)
@Service("daoFacade")
@Repository(value="daoFacade")
@Transactional // will get a javax.persistence.TransactionRequiredException: No transactional EntityManager available without it
public class ApplicationService implements ApplicationServiceLocal {
	
	//private @Value("${server}") String server; // aws only
	private Log log = LogFactory.getLog(ApplicationService.class);
	
	@PersistenceContext(name="oracle")//"${os.environment.persistencecontext.applicationservice.name}")//(unitName="entityManagerFactory", type=PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;
	
    private Map<Long,  Record> userRecordMap = new ConcurrentHashMap<>();
    private Long currentUser = 0L;
    public static final Record fakeRecord;
    static {
    	fakeRecord = new Record();
    	fakeRecord.setHeartRate1(0);
    	fakeRecord.setHeartRate2(0);
    	fakeRecord.setLongitude(0.0);
    	fakeRecord.setLattitude(0.0);
    	fakeRecord.setSendSeq(100L);
    }
    
    private Record getLatestRecordPrivate(String user) {
    	Record record = null;
        // check cache
        if(currentUser > 0) {
        	record = userRecordMap.get(Long.valueOf(user));
        	if(null != record) { 
        	    log.info("latest from cache");//: {} {}", user, record);
        	}
        }
        return record;
    }
	
    @Override
    public Boolean health() {
    	Boolean health = true;
    	// TODO: check database
    	return health;
    }
    
	@Override
	public String persist(Object aRecord) {
		String result = "OK";
		try {
			entityManager.persist(aRecord);
			log.info("persisted: " + aRecord);
		} catch (Exception e) {
			e.printStackTrace();
			result = e.getMessage();
		}
		return result;		
	}
	
	@Override
	@Transactional
	public String persist(Record aRecord) {
		return persist(aRecord, false, false);
	}
	

	@Override
	// org.springframework.transaction.CannotCreateTransactionException: Could not open JPA EntityManager for transaction; nested exception is java.lang.NullPointerException
    @Transactional
	public String persist(Record aRecord, boolean persistORM, boolean persistNoSQL) {
    	String status = "OK";
		try {
			entityManager.merge(aRecord);
			System.out.println("Commit:CMP: " + aRecord);
			// cache value
			userRecordMap.put(aRecord.getUserId(), aRecord);
			currentUser = aRecord.getUserId();
			
			if(persistORM) {
				persistORM(aRecord);
			}
			if(persistNoSQL) {
				persistNoSQL(aRecord);
			}
		} catch (Exception e) {
			e.printStackTrace();
			status = e.getMessage();
		} finally {			
		}
		return status;
	}  
    
	private void persistORM(Record aRecord) {
		// create object tree
//		User aUser = new User();
	}
	
	private void persistNoSQL(Record aRecord) {
		
	}
		
    public List<Long> getSessionIds(Long userId) {
    	return getUserIds();
    }
    
    
    @Override
    public List<Long> getUserIds() {
    	List<Long> aList = new ArrayList<>();
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<Long> query = cb.createQuery(Long.class);
	    Root<Record> target = query.from(Record.class);
	    // workaround for http://stackoverflow.com/questions/16348354/how-do-i-write-a-max-query-with-a-where-clause-in-jpa-2-0
	    SingularAttribute<? super Record, Long> anAttribute = entityManager.getMetamodel()
	    		.entity(Record.class).getSingularAttribute("userId", Long.class);
	    query.select(target.get(anAttribute));
	    query.orderBy(
	    		cb.desc(target.get(anAttribute)));
	    query.distinct(true);
	    TypedQuery<Long> typedQuery = entityManager.createQuery(query);
	    // see http://bugs.eclipse.org/303205
	    try {
	    	aList = typedQuery.getResultList();
	    } catch (NoResultException nre) {
	    	nre.printStackTrace();
	    }
	    System.out.println("list: " + aList);
		return aList;
    }

    @Override
    public String activeId() {
        // check local cache first
        //String activeId = null;
        if(currentUser > 0) {
        	return currentUser.toString();
        }
    	Long aLong = null;
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<Long> query = cb.createQuery(Long.class);
	    Root<Record> target = query.from(Record.class);
	    // workaround for http://stackoverflow.com/questions/16348354/how-do-i-write-a-max-query-with-a-where-clause-in-jpa-2-0
	    SingularAttribute<? super Record, Long> userIdAttribute = entityManager.getMetamodel()
	    		.entity(Record.class).getSingularAttribute("userId", Long.class);
	    SingularAttribute<? super Record, Long> tsStopAttribute = entityManager.getMetamodel()
	    		.entity(Record.class).getSingularAttribute("tsStop", Long.class);
	    query.select(target.get(userIdAttribute));
	    query.orderBy(
	    		cb.desc(target.get(tsStopAttribute))); // deprecated in mysql 5.7+
	    //query.distinct(true); // fix for mysql 5.7
	    TypedQuery<Long> typedQuery = entityManager.createQuery(query);
		typedQuery.setMaxResults(1);
	    // see http://bugs.eclipse.org/303205
	    try {
	    	aLong = typedQuery.getSingleResult();
	    } catch (NoResultException nre) {
	    	nre.printStackTrace();
	    }
	    System.out.println("id: " + aLong);
		return String.valueOf(aLong.longValue());    	
    }
    
    private void latestPrivate(String user, CriteriaQuery<Record> query, CriteriaBuilder cb, Root<Record> target) {
    	// special case null or 0 means any user id
    	if(null != user && !user.equalsIgnoreCase("0")) {
    		query.where(
    				cb.equal(target.get("userId"), user));
    	}
    }
    
    private TypedQuery<Record> getTypedCriteriaQuery(EntityManager entityManager, String user) {
 		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Record> query = cb.createQuery(Record.class);
		Root<Record> target = query.from(Record.class);
		// workaround for http://stackoverflow.com/questions/16348354/how-do-i-write-a-max-query-with-a-where-clause-in-jpa-2-0
		SingularAttribute<? super Record, Long> anAttribute = entityManager.getMetamodel()
    		.entity(Record.class).getSingularAttribute("tsStop", Long.class);
		query.orderBy(
    		cb.desc(target.get(anAttribute)));
		latestPrivate(user, query, cb, target);
		TypedQuery<Record> typedQuery = entityManager.createQuery(query);
		return typedQuery;	
    }
    
    @Override
    public Record latest(String user) {
    	Record result = getLatestRecordPrivate(user);
    	if(null == result) {
    		try {
    			TypedQuery<Record> typedQuery = getTypedCriteriaQuery(entityManager, user);
    			typedQuery.setMaxResults(1);
    			// see http://bugs.eclipse.org/303205
    			try {
    				result = (Record)typedQuery.getSingleResult();
    			} catch (NoResultException nre) {
    				System.out.println("No result for " + user);
    			} finally {	    	   				
    			}
    		} finally {
    		}
    		System.out.println("latest: " + user + ": " + result);
    		//System.out.println(server);
    	}
		return result;    	   	
    }

    @Override
    public Record latest() {
    	return latest(null);
    }

    @Override
    /**
     * Criteria query for the following JPQL
     * select object(r) from Record r where r.userId=USER order by r.tsStop
     */
	public List<Record> read(String user) {
    	List<Record> result = null;
    	try {
    		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    		CriteriaQuery<Record> query = cb.createQuery(Record.class);
    		Root<Record> target = query.from(Record.class);
    		// workaround for http://stackoverflow.com/questions/16348354/how-do-i-write-a-max-query-with-a-where-clause-in-jpa-2-0
    		SingularAttribute<? super Record, Long> anAttribute = entityManager.getMetamodel()
	    		.entity(Record.class).getSingularAttribute("tsStop", Long.class);
    		query.orderBy(
	    		cb.asc(target.get(anAttribute)));
    		latestPrivate(user, query, cb, target);
    		result = entityManager.createQuery(query).getResultList();
    		System.out.println("user: " + user + ": " + result.size());
    	} catch (Exception e) {
    		e.printStackTrace();
    	} finally {
    	}
    	return result;
	}
	
	@Override
	public List<Object> nativeQuery(String queryString) {
    	//EntityManager entityManager = openEntityManager();
		List<Object> list = new ArrayList<>();
		try {
			Query query = entityManager.createNativeQuery(queryString);
			list = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			//closeEntityManager(entityManager);
		}
		return list;
	}	
	
	@Override
	public String registerUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String registerDevice(Device device) {
		// TODO Auto-generated method stub
		return null;
	}


}
