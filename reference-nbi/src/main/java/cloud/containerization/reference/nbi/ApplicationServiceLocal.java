package cloud.containerization.reference.nbi;

import java.util.List;

import javax.transaction.Transactional;

import cloud.containerization.reference.entity.Device;
import cloud.containerization.reference.entity.Record;
import cloud.containerization.reference.entity.User;

//import javax.ejb.Local;
//import javax.servlet.ServletOutputStream;


//@Local
@Transactional
public interface ApplicationServiceLocal {//extends CrudRepository<Object, Long>{
	String registerUser(User user);
	String registerDevice(Device device);
	
	String persist(Object record);	
	String persist(Record record);
	String persist(Record record, boolean persistORM, boolean persistNoSQL);
	List<Record> read(String user);
	String activeId();
	Record latest(String user);
	Record latest();
	//String captureImage(ServletOutputStream out, String fullURL) throws Exception;
	List<Object> nativeQuery(String query);
	List<Long> getSessionIds(Long userId);
	@Deprecated
	List<Long> getUserIds();
	

	
	Boolean health();
}
