package cloud.containerization.reference.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
//@Table(name="SENSOR_BUILT_IN")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("B")
public class SensorBuiltIn extends Sensor {
	
}
