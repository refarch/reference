package cloud.containerization.reference.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
//@Table(name="SENSOR_TRANSIENT")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("T")
public class SensorTransient extends Sensor {
	
}
