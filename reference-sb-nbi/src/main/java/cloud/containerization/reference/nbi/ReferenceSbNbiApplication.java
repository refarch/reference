package cloud.containerization.reference.nbi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan( basePackages = {"cloud.containerization.reference.entity"} )
public class ReferenceSbNbiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReferenceSbNbiApplication.class, args);
	}

}
