package cloud.containerization.reference.nbi;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cloud.containerization.reference.entity.Record;

//@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = true)
@Service("daoFacade")
@Repository(value="daoFacade")
//@Transactional // will get a javax.persistence.TransactionRequiredException: No transactional EntityManager available without it
public class ApplicationService implements ApplicationServiceLocal {
	
	private Log log = LogFactory.getLog(ApplicationService.class);
	
	@PersistenceContext(name="${os.environment.persistencecontext.applicationservice.name}")//(unitName="entityManagerFactory", type=PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;
	
    //private Map<Long,  Record> userRecordMap = new ConcurrentHashMap<>();
    //private Long currentUser = 0L;
    public static final Record fakeRecord;
    static {
    	fakeRecord = new Record();
    	fakeRecord.setHeartRate1(0);
    	fakeRecord.setHeartRate2(0);
    	fakeRecord.setLongitude(0.0);
    	fakeRecord.setLattitude(0.0);
    	fakeRecord.setSendSeq(100L);
    }

	
    @Override
    public Boolean health() {
    	Boolean health = true;
    	// TODO: check database
    	return health;
    }
    
	@Override
	@Transactional
	public String persist(Record aRecord) {
		return persist(aRecord, false, false);
	}
	
	//@Override
	// org.springframework.transaction.CannotCreateTransactionException: Could not open JPA EntityManager for transaction; nested exception is java.lang.NullPointerException
    @Transactional
	public String persist(Record aRecord, boolean persistORM, boolean persistNoSQL) {
    	String status = "OK";
		try {
			entityManager.merge(aRecord);
			System.out.println("Commit:CMP: " + aRecord);
			// cache value
			//userRecordMap.put(aRecord.getUserId(), aRecord);
			//currentUser = aRecord.getUserId();
			
		} catch (Exception e) {
			e.printStackTrace();
			status = e.getMessage();
		} finally {			
		}
		return status;
	}  
    
    private void latestPrivate(String user, CriteriaQuery<Record> query, CriteriaBuilder cb, Root<Record> target) {
    	// special case null or 0 means any user id
    	if(null != user && !user.equalsIgnoreCase("0")) {
    		query.where(
    				cb.equal(target.get("userId"), user));
    	}
    }
    
    private TypedQuery<Record> getTypedCriteriaQuery(EntityManager entityManager, String user) {
 		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Record> query = cb.createQuery(Record.class);
		Root<Record> target = query.from(Record.class);
		// workaround for http://stackoverflow.com/questions/16348354/how-do-i-write-a-max-query-with-a-where-clause-in-jpa-2-0
		SingularAttribute<? super Record, Long> anAttribute = entityManager.getMetamodel()
    		.entity(Record.class).getSingularAttribute("tsStop", Long.class);
		query.orderBy(
    		cb.desc(target.get(anAttribute)));
		latestPrivate(user, query, cb, target);
		TypedQuery<Record> typedQuery = entityManager.createQuery(query);
		return typedQuery;	
    }
    
    @Override
    public Record latest(String user) {
    	Record result = null;//getLatestRecordPrivate(user);
    	//if(null == result) {
    		try {
    			TypedQuery<Record> typedQuery = getTypedCriteriaQuery(entityManager, user);
    			typedQuery.setMaxResults(1);
    			// see http://bugs.eclipse.org/303205
    			try {
    				result = (Record)typedQuery.getSingleResult();
    			} catch (NoResultException nre) {
    				System.out.println("No result for " + user);
    			} finally {	    	   				
    			}
    		} finally {
    		}
    		System.out.println("latest: " + user + ": " + result);
    		//System.out.println(server);
    	//}
		return result;    	   	
    }

    @Override
    public Record latest() {
    	return latest(null);
    }

    @Override
    /**
     * Criteria query for the following JPQL
     * select object(r) from Record r where r.userId=USER order by r.tsStop
     */
	public List<Record> read(String user) {
    	List<Record> result = null;
    	try {
    		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    		CriteriaQuery<Record> query = cb.createQuery(Record.class);
    		Root<Record> target = query.from(Record.class);
    		// workaround for http://stackoverflow.com/questions/16348354/how-do-i-write-a-max-query-with-a-where-clause-in-jpa-2-0
    		SingularAttribute<? super Record, Long> anAttribute = entityManager.getMetamodel()
	    		.entity(Record.class).getSingularAttribute("tsStop", Long.class);
    		query.orderBy(
	    		cb.asc(target.get(anAttribute)));
    		latestPrivate(user, query, cb, target);
    		result = entityManager.createQuery(query).getResultList();
    		System.out.println("user: " + user + ": " + result.size());
    	} catch (Exception e) {
    		e.printStackTrace();
    	} finally {
    	}
    	return result;
	}
	
    /**
     * Criteria query for the following JPQL
     * select count(1) from biometric.gps_record r where r.geohash like 'g%';
     */
	/*public List<Record> readGeoHashSubtreex(String substring) {
    	List<Record> result = null;
    	try {
    		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    		CriteriaQuery<Record> query = cb.createQuery(Record.class);
    		Root<Record> target = query.from(Record.class);
    		// workaround for http://stackoverflow.com/questions/16348354/how-do-i-write-a-max-query-with-a-where-clause-in-jpa-2-0
    		SingularAttribute<? super Record, Long> anAttribute = entityManager.getMetamodel()
	    		.entity(Record.class).getSingularAttribute("tsStop", Long.class);
    		query.orderBy(
	    		cb.asc(target.get(anAttribute)));
    		//latestPrivate(user, query, cb, target);
    	   	if(null != substring) {
        		query.where(
        				cb.like(target.get("geohash"), substring + "%"));
        	}
    		result = entityManager.createQuery(query).getResultList();
    		System.out.println("geohash: " + substring + ": " + result.size());
    	} catch (Exception e) {
    		e.printStackTrace();
    	} finally {
    	}
    	return result;
	}*/
	
	public Long readGeoHashSubtreeCount(String substring) {
    	long count = 0;
    	try {
    		TypedQuery<Long> query = entityManager.createQuery(
    				"select count(r) from Record r where r.geohash like '" + substring +"%'", Long.class);
    		count = query.getSingleResult();
    		//result = entityManager.createQuery(query).getResultList();
    		System.out.println("geohashCount: " + substring + ": " + count);
    	} catch (Exception e) {
    		e.printStackTrace();
    	} finally {
    	}
    	return count;
	}


}
