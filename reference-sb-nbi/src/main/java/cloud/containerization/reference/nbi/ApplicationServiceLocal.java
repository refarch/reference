package cloud.containerization.reference.nbi;

import java.util.List;

import cloud.containerization.reference.entity.Record;

//@Local
public interface ApplicationServiceLocal {
	String persist(Record record);
	List<Record> read(String user);
	Record latest(String user);
	Record latest();
	Boolean health();
}
