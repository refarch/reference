package cloud.containerization.reference.nbi;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@RequestMapping("/")
public class DefaultRestController {

	@Autowired
	@Qualifier("daoFacade")
    private ApplicationServiceLocal applicationServiceLocal;// = new ApplicationService();	
	
	@GetMapping("/health")
	public String getHealth() {
		return applicationServiceLocal.health().toString();
	}

}
